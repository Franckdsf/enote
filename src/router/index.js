import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login'
import Signup from '../views/Signup'
import Notes from '../views/Notes'
import Note from '../views/Note'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Notes',
    components: {
      default: Notes
    }
  },
  {
    path: '/note',
    name: 'Note',
    components: {
      default: Note
    }
  },
  {
    path: '/login',
    name: 'Login',
    components: {
      default: Login
    }
  },
  {
    path: '/signup',
    name: 'SignUp',
    components: {
      default: Signup
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
