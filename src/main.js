import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'
import router from './router'
import userStore from './store/UserStore'
import noteStore from './store/NoteStore'

Vue.config.productionTip = false
Vue.use(Vuex)

const store = new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  modules: {
    user: userStore,
    note: noteStore
  }
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
