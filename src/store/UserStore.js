import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

// jquery
const $ = require('jquery')
window.$ = $

function initUserToken () {
  if (localStorage.user_token) {
    return localStorage.user_token
  } else {
    return null
  }
}
const state = {
  login: null,
  password: null,
  token: initUserToken()
}
const getters = {
  GET_TOKEN: state => {
    return state.token
  }
}
const mutations = {
  setToken (state, token) {
    state.token = token
    localStorage.user_token = token
  }
}
const actions = {
  connect ({dispatch}, values) {
    return new Promise((resolve) => {
      var informations = {username: values.username, password: values.password}
      informations = JSON.stringify(informations)

      var connected = false

      $.ajax({type: 'POST',
        url: '//eaelectron.hol.es/api/account/',
        data: {connect: informations},
        success: function (result) {
          if (result !== 'false') {
            dispatch('setToken', result)
            connected = true
          } else {
            resolve('Identifiant ou mot de passe incorrect')
          }
        }
      })
        .always(function () {
          resolve(connected)
        })
    })
  },
  signup ({dispatch}, values) {
    return new Promise((resolve) => {
      var informations = {username: values.username, mail: values.mail, password: values.password}
      informations = JSON.stringify(informations)

      //  get all notes
      $.ajax({type: 'POST',
        url: '//eaelectron.hol.es/api/account/',
        data: {sign: informations},
        success: function (result) {
          dispatch('setToken', result)
          resolve(result)
        }
      })
    })
  },
  setToken ({commit}, token) {
    return new Promise((resolve) => {
      commit('setToken', token)
      resolve(token)
    })
  },
  disconnect ({commit}) {
    return new Promise((resolve) => {
      commit('setToken', null)
      localStorage.removeItem('user_token')
      resolve(true)
    })
  }
}

const userStore = {
  state: state,
  getters: getters,
  mutations: mutations,
  actions: actions
}

export default userStore
