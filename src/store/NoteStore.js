import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

// jquery
const $ = require('jquery')
window.$ = $

const state = {
  notes: null
}
const getters = {
  GET_NOTES: state => {
    return state.token
  }
}
const mutations = {
  setNotes (state, notes) {
    state.notes = notes
  }
}
const actions = {
  getNotes ({commit}, userToken) {
    return new Promise((resolve) => {
      $.ajax({type: 'POST',
        url: '//eaelectron.hol.es/api/notes/',
        data: {getAll: userToken},
        success: function (result) {
          if (result !== '') {
            result = JSON.parse(result)
          } else {
            result = []
          }
          commit('setNotes', result)
          resolve(result)
        }
      })
    })
  },
  setNote ({dispatch}, note) {
    return new Promise((resolve) => {
      var informations = {userToken: note.userToken, noteID: note.noteID, noteTitle: note.noteTitle, noteText: note.noteText, categorie: 1}
      informations = JSON.stringify(informations)
      console.log(informations)
      $.ajax({type: 'POST',
        url: 'http://eaelectron.hol.es/api/notes/',
        data: {setNote: informations},
        success: function () {
        }})
        .always(function (result) {
          if (result !== 'true') {
            resolve('Erreur lors de l\'enregistrement')
          } else {
            resolve('Enregistrement effectué')
            dispatch('getNotes', note.userToken)
          }
        })
    })
  },
  deleteNote ({dispatch}, data) {
    return new Promise((resolve) => {
      let informations = {userToken: data.userToken, noteID: data.noteID}
      informations = JSON.stringify(informations)

      //  get all notes
      $.ajax({type: 'POST',
        url: 'http://eaelectron.hol.es/api/notes/',
        data: {deleteNote: informations},
        success: function () {
        }})
        .always(function (result) {
          dispatch('getNotes', informations.userToken)
          resolve(result)
        })
    })
  }
}

const noteStore = {
  state: state,
  getters: getters,
  mutations: mutations,
  actions: actions
}

export default noteStore
